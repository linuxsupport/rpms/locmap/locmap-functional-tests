#!/bin/bash

function do_dir_file_check {
  echo -n "$1"
  if [ $3 == "dir" ]; then
    CHECK="-d"
  else
    CHECK="-f"
  fi
  if [ $CHECK $2 ]; then
    echo "PASS"
  else
    echo "FAIL"
    exit 1
  fi
}

function do_grep_check {
  echo -n "$1"
  grep -qri $2 $3
  if [ $? -eq 0 ]; then
    echo "PASS"
  else
    echo "FAIL"
    exit 1
  fi
}

dnf -y install locmap-release
dnf -y install locmap
# needed for later
dnf -y install rpm-build make

if [[ $1 == "all" ]]; then
  for item in $(locmap  --list |grep -v Available | awk '{print $1}' | tr '\n' ' '); do
    all_arguments+=("$item")
  done
  set -- "${all_arguments[@]}"
fi

# Initial setup
for MODULE in "$@"
do
  GIT_DIR=$(mktemp -u)
  git clone https://gitlab.cern.ch/linuxsupport/rpms/locmap/puppet-${MODULE}.git $GIT_DIR
  cd $GIT_DIR
  LATEST_BRANCH=$(git for-each-ref --sort=committerdate refs/remotes/ --format='%(refname:short)' | tail -1)
  git checkout ${LATEST_BRANCH}
  make rpm
  # Just in case the dev version does not have a spec version bump, remove it first
  rpm -e --nodeps puppet-$MODULE
  yum -y install $GIT_DIR/build/RPMS/noarch/*rpm
  if [ -f /usr/share/puppet/modules/$MODULE/linuxsupport ]; then
    locmap --enable $MODULE
  fi
done

# Run locmap
echo "locmap modules enabled:"
locmap --list
locmap --configure all --verbose
RT=$?
# locmap can return 2, "and some resources were changed"
if [ $RT -ne 0 ] && [ $RT -ne 2 ]; then
  exit $RT
fi

# Module specific tests
for MODULE in "$@"
do
  if [[ $MODULE == "afs" ]]; then
    do_dir_file_check "Checking if /afs is available: " "/afs/cern.ch" "dir"
  fi
  if [[ $MODULE == "cernbox" ]]; then
    do_dir_file_check "Checking if cernbox has been installed: " "/usr/bin/cernbox.AppImage" "file"
  fi
  if [[ $MODULE == "cernphone" ]]; then
    do_dir_file_check "Checking if cernphone has been installed: " "/usr/local/appimages/cernphone.AppImage" "file"
  fi
  if [[ $MODULE == "chrony" ]]; then
    do_grep_check "Checking if chrony has been configured: " "cern" "/etc/chrony.conf"
  fi
  if [[ $MODULE == "cvmfs" ]]; then
    do_dir_file_check "Checking if /cvmfs is available: " "/cvmfs/atlas.cern.ch" "dir"
  fi
  if [[ $MODULE == "eosclient" ]]; then
    do_dir_file_check "Checking if /eos is available: " "/eos/project/l/linux" "dir"
  fi
  if [[ $MODULE == "kerberos" ]]; then
    do_grep_check "Checking if kerbeos is sane: " "cern" "/etc/krb5.conf*"
  fi
  if [[ $MODULE == "lpadmin" ]]; then
    do_dir_file_check "Checking if lpadmin has been installed: " "/usr/sbin/lpadmincern" "file"
  fi
  if [[ $MODULE == "postfix" ]]; then
    do_grep_check "Checking if postfix is sane: " "cern" "/etc/postfix/main.cf"
  fi
  if [[ $MODULE == "resolved" ]]; then
    if [ $(rpm -E %dist) == ".el9" ]; then
      do_dir_file_check "Checking if resolved has been configured: " "/etc/systemd/system/sysinit.target.wants/systemd-resolved.service" "file"
    else
      do_dir_file_check "Checking if resolved has been configured: " "/etc/systemd/system/multi-user.target.wants/systemd-resolved.service" "file"
    fi
  fi
  if [[ $MODULE == "ssh" ]]; then
    do_grep_check "Checking if ssh has been configured: " "GSSAPIAuthentication" "/etc/ssh/sshd_config"
  fi
  if [[ $MODULE == "sudo" ]]; then
    do_dir_file_check "Checking if sudo has been configured: " "/etc/sudoers.d/000-sudo-users" "file"
  fi
  if [[ $MODULE == "zoom" ]]; then
    do_dir_file_check "Checking if zoom has been configured: " "/usr/bin/zoom" "file"
  fi
done
